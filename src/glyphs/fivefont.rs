/*
Adapted from https://github.com/ChrisG0x20/five-pixel-font

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

use super::{Font, Glyph};

const GLYPH_WIDTH: usize = 6;
const GLYPH_HEIGHT: usize = 6;

const TEXTURE_WIDTH: usize = 64;
const TEXTURE_HEIGHT: usize = 64;
const GLYPHS_PER_ROW: usize = TEXTURE_WIDTH / GLYPH_WIDTH;

pub struct FiveFont();

impl Font for FiveFont {
    #[allow(clippy::manual_range_contains)]
    fn glyph(&self, c: char) -> Option<Glyph> {
        let texture = texture();
        let c = c as usize;
        if c < 0x20 || c > 0xFE {
            return None;
        }
        let ind = c - 0x20;
        let row = ind / GLYPHS_PER_ROW;
        let col = ind % GLYPHS_PER_ROW;
        let off_x = col * GLYPH_WIDTH;
        let off_y = row * GLYPH_HEIGHT;
        let mut data = vec![vec![false; GLYPH_HEIGHT]; GLYPH_WIDTH];
        for x in 0..GLYPH_WIDTH {
            for y in 0..GLYPH_HEIGHT {
                if let Some(&val) = texture.get(off_x + x + (off_y + y) * TEXTURE_WIDTH) {
                    data[x][y] = val;
                }
            }
        }
        let space = if c == 0x20 { 5 } else { 0 };
        Some(Glyph { data, space })
    }
}

fn texture() -> Vec<bool> {
    let mut zero = false;
    let mut res = Vec::new();
    for x in DATA {
        if zero {
            res.extend_from_slice(&vec![false; (x as usize) * 8]);
            zero = false;
        } else if x == 0 {
            zero = true;
        } else {
            for i in 0..8 {
                if (x << i) & 0x80 == 0 {
                    res.push(false);
                } else {
                    res.push(true);
                }
            }
        }
    }
    while res.len() < TEXTURE_HEIGHT * TEXTURE_WIDTH {
        res.push(false);
    }
    res
}

const DATA: [u8; 437] = [
    0, 1, 133, 20, 123, 34, 8, 16, 128, 0, 1, 133, 62, 163, 69, 16, 32, 64, 0, 1, 128, 20, 112,
    130, 0, 1, 32, 64, 0, 2, 62, 41, 101, 0, 1, 32, 64, 0, 1, 128, 20, 242, 98, 128, 16, 128, 0, 8,
    168, 0, 3, 39, 8, 49, 128, 112, 128, 0, 2, 73, 152, 72, 64, 249, 192, 28, 0, 1, 138, 136, 16,
    128, 112, 130, 0, 1, 1, 12, 136, 32, 64, 168, 4, 0, 1, 34, 7, 28, 121, 128, 0, 8, 49, 194, 30,
    97, 128, 0, 1, 16, 0, 1, 81, 4, 2, 146, 66, 8, 33, 192, 121, 142, 4, 97, 192, 0, 1, 64, 0, 1,
    16, 73, 8, 144, 66, 8, 33, 192, 17, 134, 8, 96, 64, 16, 16, 0, 9, 65, 135, 28, 241, 239, 62,
    249, 224, 32, 64, 162, 138, 8, 160, 130, 0, 1, 16, 134, 190, 242, 8, 188, 242, 96, 32, 10, 162,
    138, 8, 160, 130, 32, 64, 135, 34, 241, 239, 62, 129, 224, 0, 8, 137, 195, 164, 130, 40, 156,
    241, 192, 136, 129, 40, 131, 108, 162, 138, 32, 248, 129, 48, 130, 170, 162, 242, 32, 136, 137,
    40, 130, 41, 162, 130, 64, 137, 198, 36, 242, 40, 156, 129, 160, 0, 8, 241, 239, 162, 138, 40,
    162, 113, 192, 138, 2, 34, 138, 37, 20, 17, 0, 1, 241, 194, 34, 138, 162, 8, 33, 0, 1, 160, 34,
    34, 82, 165, 8, 65, 0, 1, 147, 194, 28, 33, 72, 136, 113, 192, 0, 8, 129, 194, 0, 1, 32, 8, 0,
    1, 16, 0, 1, 64, 69, 0, 1, 17, 206, 12, 16, 128, 32, 64, 0, 1, 2, 73, 16, 113, 64, 16, 64, 0,
    1, 2, 73, 16, 145, 128, 9, 192, 62, 1, 174, 12, 112, 192, 0, 8, 16, 196, 8, 17, 2, 0, 3, 33,
    68, 0, 1, 1, 2, 52, 96, 128, 112, 199, 8, 17, 66, 42, 81, 64, 32, 68, 136, 81, 130, 34, 81, 64,
    33, 132, 136, 33, 66, 34, 80, 128, 0, 16, 96, 197, 12, 113, 37, 34, 81, 64, 81, 70, 8, 33, 37,
    42, 33, 64, 96, 196, 4, 33, 37, 42, 32, 128, 64, 68, 12, 16, 194, 20, 81, 0, 10, 66, 16, 3,
    224, 63, 128, 0, 1, 48, 130, 8, 82, 32, 63, 128, 0, 1, 17, 128, 12, 162, 32, 63, 128, 0, 1, 32,
    130, 8, 2, 32, 63, 128, 0, 1, 48, 66, 16, 3, 239, 255, 128, 0, 5, 15, 255, 128,
];
