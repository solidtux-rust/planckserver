pub mod fivefont;
pub mod tinyfont;

pub trait Font {
    fn glyph(&self, c: char) -> Option<Glyph>;
}

pub struct Glyph {
    pub data: Vec<Vec<bool>>,
    pub space: usize,
}
