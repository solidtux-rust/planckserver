# planck

Server side for serial interface of Planck keyboard

In addition to the library, a small tool (`pls`) is provided to access the functionality.

# Installation

* Install [Rust](https://rustup.rs)
* Run `cargo install --path . --force` (`--force` is only needed when updating the binary)

The serial port device is hardcoded to `/dev/ttyACM0` in the library part.

# Usage

```man
planck 0.1.0
Daniel Hauck <mail@dhauck.eu>


USAGE:
    pls <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    alarm          Alarm blinking
    color          Set full keyboard to RGB color
    help           Prints this message or the help of the given subcommand(s)
    pixel          Set single LED to RGB color
    rgb-mode       Set RGB animation mode
    rgb-restore    Restore current RGB mode
    rgb-save       Save current RGB mode
    text           Render text on keyboard
```
